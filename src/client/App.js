import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link, Redirect } from "react-router-dom";


import './app.css';
import 'material-colors/dist/colors.css';
import { Container, Row, Col, Nav, Navbar, NavDropdown, Form} from 'react-bootstrap';

import Origin from './components/Map/Origin';

import ReactImage from './react.png';

import MapPage        from './pages/MapPage';
import LeafletMapPage from './pages/LeafletMapPage';
import LoginPage      from './pages/LoginPage';
import StatsPage      from './pages/StatsPage';

export default class App extends Component {
  state = {
    current_user: {
      _id: null
    }
  };

  handleAuth(current_user){
    this.state.current_user = current_user;
    this.setState(this.state);
  }

  render() {
    return (
      <Router>
        {this.state.current_user._id && (<Navbar expand="lg">
          <Origin />

          <Nav className="mr-auto">
          </Nav>
          <NavDropdown title={<img className="user-avatar rounded-circle" src={this.state.current_user.avatar} />} id="user-menu">
            <NavDropdown.Item href="/api/users/logout">Sair</NavDropdown.Item>
          </NavDropdown>
        </Navbar>
        )}
        <Container fluid className="h-100">
          <Row className="h-100">
            <Col className="h-100 no-padding">
              <Route path="/users/sign_in"  component={(props) => <LoginPage {...props} afterAuthentication={this.handleAuth.bind(this)} />}/>
              <Route
                path="/"
                exact
                render={props =>
                  this.state.current_user._id ? (
                    <LeafletMapPage />
                  ) : (
                    <Redirect to={{pathname: "/users/sign_in", state: {from: props.location}}} />
                  )
                }
              />
              <Route
                path="/stats/report"
                exact
                render={props =>
                  this.state.current_user._id ? (
                    <StatsPage location={props.location} />
                  ) : (
                    <Redirect to={{pathname: "/users/sign_in", state: {from: props.location}}} />
                  )
                }
              />
            </Col>
          </Row>
        </Container>
      </Router>
    );
  }
}
