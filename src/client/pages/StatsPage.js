import React   from 'react'
import emitter from '../emitters/DefaultEventEmitter';
import TotalWidget  from '../components/Stats/Total';
import GenderWidget from '../components/Stats/Gender';
import AgeWidget    from '../components/Stats/Age';
import ExamWidget   from '../components/Stats/Exam';

export default class LeafletMapPage extends React.Component{
  state = {
    loading: true,
    filter:  {}
  };

  constructor(props){
    super(props);

    this.state.filter = this.props.location && this.props.location.state ? this.props.location.state.filter : {};
    this.setState(this.state);
  }
  
  componentDidMount(){
    this.addEventListener();
    window.document.querySelector('.container-fluid').className += ' sw';
  }

  addEventListener(){
    emitter.addListener('map loaded' , () => {
      this.handleAfterRequest();
    });
  }

  handleBeforeRequest(){
    this.state.loading = true;
    this.setState(this.state);
  }

  handleAfterRequest(){
    this.state.loading = false;
    this.setState(this.state);
  }

  render(){
    return (
      <div className="page h-100 container stats-page">
        <div className="row stats-row">
          <TotalWidget   filter={this.state.filter}/>
        </div>

        <div className="row stats-row">
          <h2 className="stats-row-name">Sexo</h2>
          <GenderWidget  filter={this.state.filter}/>
        </div>

        <div className="row stats-row stats-age-row">
          <h2 className="stats-row-name">Idade</h2>
          <AgeWidget  filter={this.state.filter}/>
        </div>

        <div className="row stats-row stats-age-row">
          <h2 className="stats-row-name">Triglicérides</h2>
          <ExamWidget name="triglicerides" url="/api/stats/report/triglicerides" type="column" filter={this.state.filter}/>
        </div>

        <div className="row stats-row stats-age-row">
          <h2 className="stats-row-name">LDL</h2>
          <ExamWidget name="ldl" url="/api/stats/report/colesterol_ldl" type="column" filter={this.state.filter}/>
        </div>

        <div className="row stats-row stats-age-row">
          <h2 className="stats-row-name">HDL</h2>
          <ExamWidget name="hdl" url="/api/stats/report/colesterol_hdl" type="column" filter={this.state.filter}/>
        </div>

        <div className="row stats-row stats-glicemia-row">
          <h2 className="stats-row-name">GLICEMIA</h2>
          <ExamWidget name="glicemia" url="/api/stats/report/glicemia_jejum" type="column" filter={this.state.filter}/>
        </div>

        <div className="row stats-row stats-age-row">
          <h2 className="stats-row-name">HEMOGLOBINA GLICADA</h2>
          <ExamWidget name="hemoglobina-glicada" url="/api/stats/report/hemoglobina_glicada" type="column" filter={this.state.filter}/>
        </div>

        <div className="row stats-row stats-age-row">
          <h2 className="stats-row-name">IMC</h2>
          <ExamWidget name="imc" url="/api/stats/report/bmi" type="column" filter={this.state.filter}/>
        </div>

        <div className="row stats-row stats-age-row">
          <h2 className="stats-row-name">PRESSÃO ARTERIAL SISTÓLICA</h2>
          <ExamWidget name="pressao-arterial-sistolica" url="/api/stats/report/clinical_notes_pressao_sistolica" type="column" filter={this.state.filter}/>
        </div>

        <div className="row stats-row stats-age-row">
          <h2 className="stats-row-name">PRESSÃO ARTERIAL DIASTÓLICA</h2>
          <ExamWidget name="pressao-arterial-diastolica" url="/api/stats/report/clinical_notes_pressao_diastolica" type="column" filter={this.state.filter}/>
        </div>
      </div>
    );
  }
}

