import React       from 'react'
import GoogleLogin from 'react-google-login';
import axios       from 'axios';

import Logo from '../images/logo.png';
import Loading   from '../images/loading.svg';

export default class LoginPage extends React.Component{
  state = {
    loading:      false,
    current_user: {},
    message:      false,
    logged_in: null
  };

  componentDidMount(){
    this.loggedIn();
  }

  loggedIn(){
    this.state.loading = true;

    this.setState(this.state);
    axios.get('/api/users/logged_in')
         .then((result) => {
            this.state.loading = false;

            if(result.data.status === false){
              this.state.logged_in    = false;
              this.state.message = result.data.message;
              return this.setState(this.state);
            }

            this.state.logged_in    = true;
            this.state.current_user = result.data;
            // this.setState(this.state);
            this.handleEventListen(this.state.current_user);
         });
  }

  handleSuccess(response){
    this.state.loading = true;

    this.setState(this.state);
    axios.post('/api/users/sign_in', response)
         .then((result) => {
            this.state.loading = false;

            if(result.data.status === false){
              this.state.message = result.data.message;
              return this.setState(this.state);
            }

            this.state.logged_in    = true;
            this.state.current_user = result.data;
            this.setState(this.state);
            this.handleEventListen(this.state.current_user);
         });
  }

  handleFailure(response){
    //
  }

  handleEventListen(current_user){
    console.log(this.state, this.props);
    setTimeout(() => {
      this.props.afterAuthentication(current_user);
      this.props.history.push(this.props.location.state.from.pathname);
    });
  }

  render(){
    return (
      <div className="page page-login h-100">
        <div className="screen">
          <h3>Sign In</h3>
          <img src={Logo} className="logo" alt="HSP Map"/>

          {!this.state.loading && 
            <GoogleLogin
              clientId="759373078024-dkj2uirefkmgjt87r8onvu0r7f35lgie.apps.googleusercontent.com"
              theme='dark'
              onSuccess={this.handleSuccess.bind(this)}
              onFailure={this.handleFailure.bind(this)}
            />
          }
          {this.state.loading && <div className="d-block"><img  src={Loading} alt="Autenticando" /></div>}
        </div>
      </div>
    );
  }
}