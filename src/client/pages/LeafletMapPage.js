import React from 'react'
// import Map       from '../components/Map/Map';
import Map       from '../components/Leaflet/Map';
import Filter    from '../components/Map/Filter';
import Preloader from '../components/Utils/Preloader';
import Counter   from '../components/Utils/Counter';
import Graph     from '../components/Graph/Bar';

import emitter   from '../emitters/DefaultEventEmitter';

export default class LeafletMapPage extends React.Component{
  state = {
    loading: true
  };
  
  componentDidMount(){
    this.addEventListener();
  }

  addEventListener(){
    emitter.addListener('map loaded' , () => {
      this.handleAfterRequest();
    });
  }

  handleBeforeRequest(){
    this.state.loading = true;
    this.setState(this.state);
  }

  handleAfterRequest(){
    this.state.loading = false;
    this.setState(this.state);
  }

  render(){
    return (
      <div className="page page-map h-100">
        <Counter/>
        <Map />        
        <Filter
          onBeforeRequest={this.handleBeforeRequest.bind(this)}
          onAfterRequest={this.handleAfterRequest.bind(this)}
        />

        <Graph />
      </div>
    );
  }
}

