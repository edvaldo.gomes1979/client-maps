import React from 'react'
import Map       from '../components/Map/Map';
import Filter    from '../components/Map/Filter';
import Preloader from '../components/Utils/Preloader';

import emitter   from '../emitters/DefaultEventEmitter';

export default class MapPage extends React.Component{
  constructor(props){
    super(props);

    this.state = {
      loading: true
    };

    this.addEventListener();
  }

  addEventListener(){
    emitter.addListener('map loaded' , () => {
      this.handleAfterRequest();
    });
  }

  handleBeforeRequest(){
    console.log('123');
    this.state.loading = true;
    this.setState(this.state);
  }

  handleAfterRequest(){
    this.state.loading = false;
    this.setState(this.state);
  }

  render(){
    return (
      <div className="page page-map h-100">
        {this.state.loading && <Preloader fullPage />}
        <Filter
          onBeforeRequest={this.handleBeforeRequest.bind(this)}
          onAfterRequest={this.handleAfterRequest.bind(this)}
        />
        <Map />
      </div>
    );
  }
}

