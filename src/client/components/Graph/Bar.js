import React from 'react'

import emitter from '../../emitters/DefaultEventEmitter';
import GraphIcon from '../../images/icons/bars-chart.svg';
import Loading   from '../../images/loading.svg';
import { Modal, Button } from 'react-bootstrap';
import { Redirect } from "react-router-dom";
import axios from 'axios';

class Graph extends React.Component{
  constructor(props){
    super(props);

    this.state = {
      show: false,
      groups: [],
      loading: false
    }

    this.addEventListener();
  }

  addEventListener(){
    emitter.addListener('change filter', (filter) => {
      this.state.filter = filter;
    });
  }

  load(filter, addEventListener){
    this.state.loading = true;

    filter = Object.assign({}, {type: 'graph'}, filter);
    
    this.setState(this.state);
    axios.get('/api/exam/regions?filter=' + JSON.stringify(filter))
         .then((result) => {
            this.state.loading = false;
            this.setState(this.state);

            addEventListener(result.data);
         });
  }

  handleClose(){
    this.state.show = false;
    this.setState(this.state);
  }

  handleShow(){
    this.state.show = true;

    this.setState(this.state);
    return;
    this.load(this.state.filter, (data) => {
      this.state.groups = data;
      this.state.show = true;

      this.setState(this.state);
    });
  }

  renderChart(){
    Highcharts.chart('graph', {
      chart: {
        type: 'column',
        options3d: {
          enabled: true,
          alpha: 15,
          beta: 5,
          depth: 50,
          viewDistance: 25
        }
      },
      title: {
        text: ''
      },
      plotOptions: {
          series: {
              borderWidth: 0,
              dataLabels: {
                  enabled: false                       
              }
          }
      }, 
      xAxis: {
        type: 'category'
      },
      yAxis: {
        title: {
          text: ''
        }
      },
      series: [{
        name: this.state.groups.type,
        colorByPoint: true,
        data: this.state.groups.data
      }],

      credits: {
        enabled: false
      },
    });
  }

  render(){
    return (
      <>
        <div className="graph graph-button" onClick={this.handleShow.bind(this)}>
          {!this.state.loading && <img src={GraphIcon} alt="Grafico"/>}
          {this.state.loading && <img className="loading" src={Loading} alt="Carregando Grafico" />}
        </div>


        {this.state.show && <Redirect to={{pathname: '/stats/report', state: {filter: this.state.filter}}}  />}
        // <Modal className="graph-box" size="lg" show={this.state.show} onHide={this.handleClose.bind(this)} onEntered={this.renderChart.bind(this)}>
        //   <Modal.Body>
        //     <div id="graph"></div>
        //   </Modal.Body>
        //   <Modal.Footer>
        //     <Button variant="secondary" onClick={this.handleClose.bind(this)}>
        //       Fechar
        //     </Button>
        //   </Modal.Footer>
        // </Modal>
      </>
    );
  }
}

export default Graph;