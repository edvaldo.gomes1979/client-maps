import React from 'react'
import emitter from '../../emitters/DefaultEventEmitter';

import { withScriptjs, withGoogleMap, GoogleMap, Marker} from "react-google-maps"
import HeatmapLayer from "react-google-maps/lib/components/visualization/HeatmapLayer";


// function Table(props) {
//   const [show, setShow] = useState(false);

//   const handleClose = () => setShow(false);
//   const handleShow = () => setShow(true);

//   return (
//     <>
//       <Button variant="primary" onClick={handleShow}>
//         Bairros
//       </Button>

//       <Modal show={show} onHide={handleClose}>
//         <Modal.Header closeButton>
//           <Modal.Title>Bairros</Modal.Title>
//         </Modal.Header>
//         <Modal.Body>

//         </Modal.Body>
//         <Modal.Footer>
//           <Button variant="secondary" onClick={handleClose}>
//             Fechar
//           </Button>
//         </Modal.Footer>
//       </Modal>
//     </>
//   );
// }

const Map = withScriptjs(withGoogleMap((props) => {
  props.onMapRender();
  let bounds = google.maps;
  return (
    <GoogleMap
      defaultZoom={16}
      defaultCenter={{ lat: -23.533773, lng: -46.625290 }}
      options={{
        mapTypeControlOptions: {
          style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
          position: google.maps.ControlPosition.BOTTOM_CENTER
        },
        zoomControlOptions: {
          position: google.maps.ControlPosition.LEFT_BOTTOM
        },
        streetViewControl: false,
        fullscreenControl: false
      }}
      onBoundsChanged={function(i){
        props.onBoundsChanged(this.getBounds());
      }}
      heatmapLibrary={true}
    >
      <HeatmapLayer data={props.heatmapData} opacity="1"/>
    </GoogleMap>
  );
}));

export default class MapContainer extends React.Component{  
  constructor(props){
    super(props);

    this.state = {
      data: []
    }

    this.addEventListener();
  }

  addEventListener(){
    emitter.addListener('set locations', (data) => {
      this.state.data = data.map((i) => {
        return {
          location: new google.maps.LatLng(i.lat, i.lon),
          weight: 1,
        };
      });
      this.setState(this.state);
    });
  }

  handleMapRender(){
    // console.log('Hi');
    // emitter.emit('load data', true);
  }

  handleBoundsChange(bounds){
    emitter.emit('change bounds', bounds);
  }

  render(){
    return (
      <Map
        googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyDI1r8t1nPzHHpX9TdS7SeJvUFv4f9FNMI&callback=initMap&libraries=visualization"
        loadingElement={<div style={{ height: `100%` }} />}
        containerElement={<div style={{ height: `100%` }} />}
        mapElement={<div style={{ height: `100%` }} />}
        heatmapData={this.state.data}
        onMapRender={this.handleMapRender.bind(this)}
        onBoundsChanged={this.handleBoundsChange.bind(this)}
      />
    );
  }
}

