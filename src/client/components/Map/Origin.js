import React, { Component } from 'react';

import { ButtonToolbar, ToggleButtonGroup, ToggleButton} from 'react-bootstrap';

import emitter from '../../emitters/DefaultEventEmitter';

export default class Origin extends Component{
  state = {
    origin: []
  };

  handleChange(selected){
    this.state.origin = selected;
    this.setState(this.state);

    emitter.emit('change origin', this.state.origin);
  }
  
  render(){
    return (
      <ButtonToolbar className="origin">
        <ToggleButtonGroup type="checkbox" defaultValue={['fleury_others', 'fleury_checkup', 'hsp']} onChange={this.handleChange.bind(this)}>
          <ToggleButton type="button" variant="light" size="sm" value='fleury_others'>Fleury</ToggleButton>
          <ToggleButton type="button" variant="light" size="sm" value='fleury_checkup'>Fleury Checkup</ToggleButton>
          <ToggleButton type="button" variant="light" size="sm" value='hsp'>HSP</ToggleButton>
        </ToggleButtonGroup>
      </ButtonToolbar>
    );
  }
}