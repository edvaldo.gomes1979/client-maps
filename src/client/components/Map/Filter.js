import React   from 'react';
import { Form, ButtonToolbar,  ToggleButtonGroup, ToggleButton} from 'react-bootstrap';

import emitter from '../../emitters/DefaultEventEmitter';
import axios   from 'axios';
import httpAdapter from 'axios/lib/adapters/http';
import oboe        from 'oboe';

import FiltersOptions from './filterOptions';

import 'animate.css';

const FilterItem = (props) => {
  const active = (option, filter) => {
    if(props.hasSelected(option, filter)){
      return ' active';
    }
    return '';
  };

  const enableExam = (filter) => {
    if(filter.category === 'exam' && !props.enableExam(filter)){
      return ' disable';
    }

    return '';
  };

  return (
    <div className={'map-filter-item ' + enableExam(props.filter)}>
      <p className="font-weight-bold m-0 label text-uppercase">{props.filter.label}</p>
      <ul className="options">
        {props.filter.options.map((option, i) => 
          <li
            className={'option d-inline-block ' + (option.color ? option.color : props.filter.color) + active(option, props.filter)}
            key={i}
            onClick={() => { return props.handleFilterToggle(option, props.filter)}}
          >
            {option.label ? option.label : option.value}
          </li>
        )}
      </ul>
    </div>
  );
};

export default class Filter extends React.Component{
  constructor(props){
    super(props);
    
    this.request = {
      source: null,
      chunks: 0
    };
    
    this.state = {
      displayPoints: false,

      filter: {
        exam:           {},
        clinical_notes: {},
        bounds:         {}
      },
      style: {
        className: '',
        status:    true
      }
    };
    
    this.addEventListener();
  }

  addEventListener(){
    emitter.addListener('ready map', (data) => {
      this.state.filter.bounds = data.bounds;
      this.loadRegions(this.state.filter);
    });

    emitter.addListener('change bounds', (data) => {
      this.state.filter.bounds = data.bounds;
      this.state.filter.zoom   = data.zoom;

      emitter.emit('change filter', this.state.filter);
    });

    emitter.addListener('change origin', (origin) => {
      this.state.filter.origin = origin;
      this.loadRegions(this.state.filter);
    });
  }

  load(filter){
    emitter.emit('change filter', filter)

    this.cancelLoad();

    this.request.chunks  = 0;
    this.request.source = oboe('/api/exam/results?filter=' + JSON.stringify(filter)).done((data) => {
      this.response(data);
    })
    .fail(() => {
      console.log('fail!');
    });
  }

  loadRegions(filter){
    this.cancelLoad();

    emitter.emit('change filter', filter)
    emitter.emit('start loading');

    this.request.source = axios.CancelToken.source();
    axios.get('/api/regions/geometries?filter=' + JSON.stringify(filter), {cancelToken: this.request.source.token})
         .then((result) => {
            emitter.emit('set regions', result.data.regions);
            emitter.emit('set total',   result.data.total);
            emitter.emit('end loading');
         })

  }

  cancelLoad(){
    if(!this.request.source){
      return;
    }

    this.request.source.cancel();
  }

  response(data){
    this.request.chunks += 1;
    
    if(this.request.chunks === 1){
      return emitter.emit('set locations', data);
    }

    return emitter.emit('add locations', data);
  }

  handleFilterToggle(option, filter){
    if(!this.hasSelected(option, filter)){
      this.selectFilter(option, filter);
    }else{
      this.removeFilter(option, filter)
    }

    this.loadRegions(this.state.filter);
  }

  selectFilter(option, filter){
    if(filter.category === 'exam'){
      option.filter_name = filter.name;

      this.state.filter.exam[filter.name] = this.state.filter.exam[filter.name] ? this.state.filter.exam[filter.name] : [];
      this.state.filter.exam[filter.name].push(option);
    }

    if(filter.category === 'clinical_notes'){
      this.state.filter.clinical_notes[filter.name] = this.state.filter.clinical_notes[filter.name] ? this.state.filter.clinical_notes[filter.name] : [];
      this.state.filter.clinical_notes[filter.name].push(option)
    }

    this.setState(this.state);
  }

  removeFilter(option, filter){
    let index;

    if(filter.category === 'exam'){
      index = this.getSelectedIndex(this.state.filter.exam[filter.name], option.label);
      this.state.filter.exam[filter.name].splice(index, 1);
    }

    if(filter.category === 'clinical_notes'){
      index = this.getSelectedIndex(this.state.filter.clinical_notes[filter.name], option.label);
      this.state.filter.clinical_notes[filter.name].splice(index, 1);
    }

    this.setState(this.state);
  }

  hasSelected(option, filter){
    if(
      filter.category === 'exam'          &&
      this.state.filter.exam              &&
      this.state.filter.exam[filter.name] &&
      this.getSelectedIndex(this.state.filter.exam[filter.name], option.label) !== -1
    ){
      return true;
    }

    if(
      !this.state.filter.clinical_notes[filter.name] ||
      this.getSelectedIndex(this.state.filter.clinical_notes[filter.name], option.label) === -1
    ){
      return false;
    }

    return true;
  }

  getSelectedIndex(data, label){
    return data.map((i) => { return i.label })
               .indexOf(label);
  }

  enableExam(filter){
    return true;
    if(Object.keys(this.state.filter.exam).length === 0){
      return true;
    }

    if(
      this.state.filter.exam &&
      this.state.filter.exam.filter_name === filter.name
    ){
      return true;
    }

    return false;
  }

  toggle(){
    this.state.style.status = !this.state.style.status;
    this.state.style.className = this.state.style.status ? 'opened' : 'closed';

    this.setState(this.state);
  }

  handleTogglePoints(){
    this.state.displayPoints = !this.state.displayPoints;

    emitter.emit('display points', this.state.displayPoints);
    this.setState(this.state); 
  }

  render(){
    return (
      <div className={'map-filter ' + this.state.style.className}>
        <span className="btn" onClick={this.toggle.bind(this)}></span>
        {FiltersOptions.map((filter, i) => 
          <FilterItem
            filter={filter}
            key={i}
            handleFilterToggle={this.handleFilterToggle.bind(this)}
            hasSelected={this.hasSelected.bind(this)}
            enableExam={this.enableExam.bind(this)}
          />
        )}

         <ButtonToolbar className="mt-3">
          <ToggleButtonGroup type="radio" name="options" className="points-group" onChange={this.handleTogglePoints.bind(this)}>
            <ToggleButton size="sm" className={this.state.displayPoints ? 'btn-points active' : 'btn-points'} active={this.state.displayPoints} >Unidades Fleury</ToggleButton>
          </ToggleButtonGroup>
        </ButtonToolbar>
      </div>
    );
  }
}