import React   from 'react'
import emitter from '../../emitters/DefaultEventEmitter';
import axios   from 'axios';

export default class Exam extends React.Component{
  state = {
    loading: false,
    data:    {}
  };


  componentDidMount(){
    this.load();
  }

  addEventListener(){
    emitter.addListener('stats page loaded' , () => {
      this.load();
    });
  }

  load(){
    let url = `${this.props.url}?filter=${JSON.stringify(this.props.filter)}`;

    this.state.loading = true;
    this.setState(this.state);

    axios.get(url)
         .then((result) => {
            this.state.loading = false;
            this.state.data    = result.data;
            
            this.setState(this.state);
            this.makeGraph();
         });
  }

  makeGraph(){
    Highcharts.chart(`graph-${this.props.name}`, {
      chart: {
        type: this.props.type
        // type: 'column',
        // options3d: {
        //   enabled: true,
        //   alpha: 15,
        //   beta: 5,
        //   depth: 50,
        //   viewDistance: 25
        // }
      },
      title: {
        text: ''
      },
      plotOptions: {
          series: {
              borderWidth: 0,
              dataLabels: {
                  enabled: false                       
              }
          }
      }, 
      xAxis: {
        type: 'category',
        categories: this.state.data.categories
      },
      yAxis: {
        title: {
          text: ''
        }
      },
      series: this.state.data.series,

      credits: {
        enabled: false
      },
    });
  }

  render(){
    return (
      <>
        <div className="stats-graph stats-age" id={'graph-' + this.props.name}></div>
      </>
    );
  }
};

// {
//         name: ['Feminino', 'Masculino'],
//         colorByPoint: true,
//         data: [300, 300]
//       }