import React   from 'react'
import emitter from '../../emitters/DefaultEventEmitter';

export default class Total extends React.Component{
  state = {
    genders: [
      {
        name: 'Fleury',
        data: [
          {
            name:  'Feminino',
            color: '#ec407a',
            total: 15
          },
          {
            name:  'Masculino',
            color: '#29b6f6',
            total: 25
          }
        ]
      },
      {
        name: 'Fleury Checkup',
        data: [
          {
            name:  'Feminino',
            color: '#ec407a',
            total: 152
          },
          {
            name:  'Masculino',
            color: '#29b6f6',
            total: 251
          }
        ]
      },
      {
        name: 'HSP',
        data: [
          {
            name:  'Feminino',
            color: '#ec407a',
            total: 1252
          },
          {
            name:  'Masculino',
            color: '#29b6f6',
            total: 125
          }
        ]
      }
    ]
  };


  componentDidMount(){
    this.load();
  }

  addEventListener(){
    emitter.addListener('stats page loaded' , () => {
      this.load();
    });
  }

  load(){
    this.makeGraph();
  }

  makeGraph(){
    Highcharts.chart('stats-graph', {
      chart: {
        type: 'column',
        // options3d: {
        //   enabled: true,
        //   alpha: 15,
        //   beta: 5,
        //   depth: 50,
        //   viewDistance: 25
        // }
      },
      title: {
        text: ''
      },
      plotOptions: {
          series: {
              borderWidth: 0,
              dataLabels: {
                  enabled: false                       
              }
          }
      }, 
      xAxis: {
        type: 'category',
        categories: ['FLEURY', 'FLEURY CHECKUP', 'HSP']
      },
      yAxis: {
        title: {
          text: ''
        }
      },
      series: [
        {
          name: 'Masculino',
          data: [130, 454, 35],
          color: '#ec407a'
        },
        {
          name: 'Feminino',
          data: [122, 11, 544],
          color: '#29b6f6'
        }
      ],

      credits: {
        enabled: false
      },
    });
  }

  render(){
    return (
      <>
        <div className="stats-graph stats-gender" id="stats-graph"></div>
      </>
    );
  }
};

// {
//         name: ['Feminino', 'Masculino'],
//         colorByPoint: true,
//         data: [300, 300]
//       }