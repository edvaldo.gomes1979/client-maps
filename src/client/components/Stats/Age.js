import React   from 'react'
import emitter from '../../emitters/DefaultEventEmitter';

export default class Age extends React.Component{
  state = {
    genders: [
      {
        name: 'Fleury',
        data: [
          {
            name:  'Feminino',
            color: '#ec407a',
            total: 15
          },
          {
            name:  'Masculino',
            color: 'rgba(249, 168, 37, 1)',
            total: 25
          }
        ]
      },
      {
        name: 'Fleury Checkup',
        data: [
          {
            name:  'Feminino',
            color: '#ec407a',
            total: 152
          },
          {
            name:  'Masculino',
            color: 'rgba(249, 168, 37, 1)',
            total: 251
          }
        ]
      },
      {
        name: 'HSP',
        data: [
          {
            name:  'Feminino',
            color: '#ec407a',
            total: 1252
          },
          {
            name:  'Masculino',
            color: 'rgba(249, 168, 37, 1)',
            total: 125
          }
        ]
      }
    ]
  };


  componentDidMount(){
    this.load();
  }

  addEventListener(){
    emitter.addListener('stats page loaded' , () => {
      this.load();
    });
  }

  load(){
    this.makeGraph();
  }

  makeGraph(){
    Highcharts.chart('stats-age-graph', {
      chart: {
        type: 'bar',
        // type: 'column',
        // options3d: {
        //   enabled: true,
        //   alpha: 15,
        //   beta: 5,
        //   depth: 50,
        //   viewDistance: 25
        // }
      },
      title: {
        text: ''
      },
      plotOptions: {
          series: {
              borderWidth: 0,
              dataLabels: {
                  enabled: false                       
              }
          }
      }, 
      xAxis: {
        type: 'category',
        categories: ['FLEURY', 'FLEURY CHECKUP', 'HSP']
      },
      yAxis: {
        title: {
          text: ''
        }
      },
      series: [
        {
          name: '0 - 45',
          data: [130, 454, 35],
          color: 'rgba(249, 168, 37, 0.2)'
        },
        {
          name: '46 - 65',
          data: [12, 111, 44],
          color: 'rgba(249, 168, 37, 0.4)'
        },
        {
          name: '66 - 80',
          data: [12, 111, 44],
          color: 'rgba(249, 168, 37, 0.6)'
        },
        {
          name: '81 - 90',
          data: [12, 111, 54],
          color: 'rgba(249, 168, 37, 0.8)'
        },
        {
          name: '> 90',
          data: [12, 111, 524],
          color: 'rgba(249, 168, 37, 1)'
        }
      ],

      credits: {
        enabled: false
      },
    });
  }

  render(){
    return (
      <>
        <div className="stats-graph stats-age" id="stats-age-graph"></div>
      </>
    );
  }
};

// {
//         name: ['Feminino', 'Masculino'],
//         colorByPoint: true,
//         data: [300, 300]
//       }