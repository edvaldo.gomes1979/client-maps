import React   from 'react'
// import emitter from '../emitters/DefaultEventEmitter';

export default class Total extends React.Component{
  state = {
    totals: [
      {
        name: 'Fleury',
        total: '550.012'
      },
      {
        name: 'Fleury Checkup',
        total: '30.012'
      },
      {
        name: 'HSP',
        total: '425.012'
      }
    ]
  };


  componentDidMount(){
    // this.addEventListener();
  }

  addEventListener(){
    emitter.addListener('stats page loaded' , () => {
      this.load();
    });
  }

  render(){
    return (
      <>
        {this.state.totals.map((stats, i) => 
          <div className="stats stats-metric" key={i}>
            <h3 className="stats-value">{stats.total}</h3>
            <h5 className="stats-name">{stats.name}</h5>
          </div>
        )}
      </>
    );
  }
};