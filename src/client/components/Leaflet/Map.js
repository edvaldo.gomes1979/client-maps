import React from 'react'
import numeral from 'numeral';

import emitter from '../../emitters/DefaultEventEmitter';
import 'leaflet.heat';

import Point from '../../images/icon-fleury.png';

import axios from 'axios';

export default class MapContainer extends React.Component{ 
  state = {
    data:   [],
    places: []
  };

  addEventListener(){
    emitter.addListener('set locations', (data) => {
      this.heandleSetPoits(data);
    });

    emitter.addListener('set regions', (data) => {
      this.regions(data);
    });

    emitter.addListener('add locations', (data) => {
      this.heandleAddPoits(data);
    });

    emitter.addListener('display points', (data) => {
      this.loadPlaces(data);
    });

    this.map.whenReady((e) => {
      emitter.emit('ready map', {bounds: this.map.getBounds(), zoom: this.map.getZoom()});
    });

    this.map.on('moveend', (e) => {
      emitter.emit('change bounds', {bounds: this.map.getBounds(), zoom: this.map.getZoom()});
    });
  }

  componentDidMount(){
    this.marker = {};
    this.accessToken = 'pk.eyJ1Ijoib2x1Y2FzYWx2ZXMiLCJhIjoiY2p6OGR3YWtqMDEwaTNobG8wYTh3MW4zZiJ9.N7Ywg1VrGiMYF7v40YJ1Dw';
    this.map = L.map('map').setView([-23.533773, -46.625290], 13);
    this.addEventListener();

    const Layout = 'https://api.mapbox.com/styles/v1/olucasalves/cjza2momd56u71cnrymh66ma8/tiles/256/{z}/{x}/{y}?access_token=' + this.accessToken;

    L.tileLayer(Layout, {
      id: 'mapbox.light',
      attribution: '',
    }).addTo(this.map);
    
    this.heat = L.heatLayer([], {radius: 25, gradient: {0.5: '#29b6f6', 0.6: 'lime', 0.7: '#ef5350'}}).addTo(this.map);
    this.map.createPane('labels');
    this.map.getPane('labels').style.zIndex = 599;

    L.tileLayer('https://{s}.basemaps.cartocdn.com/light_only_labels/{z}/{x}/{y}.png', {
        attribution: '',
        pane: 'labels'
    }).addTo(this.map);

    this.displayLegend();
    this.displayInfo();
  }

  regions(data){    
    const style = (feature) => {
      return {
        fillColor: this.getColor(feature.properties.density),
        weight: 2,
        opacity: 1,
        color: '#FFFFFF',
        dashArray: '3',
        fillOpacity: 0.6
      }
    };

    const highlightFeature = (e) => {
      var layer = e.target;

      layer.setStyle({
          weight: 2,
          color: '#3BAFDA',
          dashArray: '',
          fillOpacity: 0.7
      });

      if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
        layer.bringToFront();
      }

      this.info.update(layer.feature.properties);
    };

    const resetHighlight = (e) => {
      this.geojson.resetStyle(e.target);
      this.info.update();
    }

    const zoomToFeature = (e) =>{
      this.map.fitBounds(e.target.getBounds());
    }

    const onEachFeature = (feature, layer) => {
      layer.on({
        mouseover: highlightFeature,
        mouseout:  resetHighlight,
        click:     zoomToFeature
      });
    };

    if(this.geojson){
      this.map.removeLayer(this.geojson);
    }

    this.geojson = L.geoJson(data, {style: style, onEachFeature: onEachFeature});
    this.geojson.addTo(this.map);
  }

  displayInfo(){
    this.info = L.control({position: 'bottomleft'});

    this.info.onAdd = function (map) {
      this._div = L.DomUtil.create('div', 'info');
      this.update();
      return this._div;
    };

    this.info.update = function (i) {
      this._div.innerHTML = '' +  (i ?
        '<b>' + i.name + '</b><br />' + numeral(i.density).format() + ' pacientes'
        : '');
    };

    this.info.addTo(this.map);
  }

  displayLegend(){
    let legend = L.control({position: 'bottomleft'});

    legend.onAdd = (map) => {
        var div = L.DomUtil.create('div', 'info legend'),
            grades = [0, 100, 200, 500, 1000, 2000, 5000, 10000],
            labels = [];

        for (var i = 0; i < grades.length; i++) {
            div.innerHTML +=
                '<i style="background:' + this.getColor(grades[i] + 1) + '"></i> ' +
                numeral(grades[i]).format() + (grades[i + 1] ? ' &ndash; ' + numeral(grades[i + 1]).format() + '<br>' : '+');
        }

        return div;
    };

    legend.addTo(this.map);
  }

  getColor(d){
    return d > 10000 ? '#800026' :
      d > 5000  ? '#BD0026' :
      d > 2000  ? '#E31A1C' :
      d > 1000  ? '#FC4E2A' :
      d > 500   ? '#FD8D3C' :
      d > 200   ? '#FEB24C' :
      d > 100   ? '#FED976' : '#FFEDA0';
  };

  loadPlaces(display){
    if(!display){
      let i;
      for(i in this.marker){
        this.map.removeLayer(this.marker[i]);
      }

      return;
    }

    if(this.state.places.length){
      return this.handleMarkers(this.state.places); 
    }

    axios.get('/api/exam/places')
         .then((result) => {
            this.state.places = result.data; 
            this.handleMarkers(this.state.places);
         });
  }

  handleMarkers(places){
    let icon = L.icon({
      iconUrl: Point,
      iconSize:     [25, ]
    });

    places.map((place, i) => {
      this.marker[i] = L.marker([place.lat, place.lon], {icon: icon}).addTo(this.map);
      this.marker[i].bindPopup(`<b>${place.name}</b><br>${place.address}`);
    });
  }

  heandleSetPoits(message){
    this.heat.setLatLngs(message.data);
  }

  heandleAddPoits(message){
    message.data.map((item) => {
      this.heat.addLatLng(item);
    });

    this.heat.redraw();
  }

  handleMapRender(){
    // console.log('Hi');
    // emitter.emit('load data', true);
  }

  handleBoundsChange(bounds){
    emitter.emit('change bounds', bounds);
  }

  render(){
    return (
      <div id="map">
      </div>
    );
  }
}

