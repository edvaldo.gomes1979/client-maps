import React from 'react'
import {Zoom, Sugar} from 'react-preloaders';


import emitter from '../../emitters/DefaultEventEmitter';
import numeral from 'numeral';

numeral.register('locale', 'pt-BR', {
    delimiters: {
        thousands: '.',
        decimal: ','
    },
    abbreviations: {
        thousand: 'k',
        million: 'm',
        billion: 'b',
        trillion: 't'
    },
    ordinal : function (number) {
        return number === 1 ? 'er' : 'ème';
    },
    currency: {
        symbol: 'R$'
    }
});

// switch between locales
numeral.locale('pt-BR');

class Counter extends React.Component{
  state = {
    loading: 0,

    current: 0,
    total:   0,
    
    html: {
      current: '',
      total:   '',
    }
  };

  componentDidMount(props){
    emitter.addListener('set total', (data) => {
      this.set(data);
    });

    emitter.addListener('start loading', (data) => {
      this.state.loading = true;
      this.setState(this.state);
    });

    emitter.addListener('end loading', (data) => {
      this.state.loading = false;
      this.setState(this.state);
    });

  }

  set(total){
    // this.state.current = data.chunk;
    this.state.total   = total;

    // this.state.html.current = numeral(data.chunk).format();
    this.state.html.total = numeral(total).format();
    this.setState(this.state);
  }

  render(){
    return (
      <div className="counter">
        <span className="total">{this.state.html.total}</span>
        {this.state.loading && <Zoom/>}
      </div>
    );
  }
}

export default Counter;