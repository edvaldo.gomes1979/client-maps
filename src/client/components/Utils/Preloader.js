import React from 'react'
import { Planets, Sugar } from 'react-preloaders';

const Preloader = (props) => {
  return (
    <div className={'preloader ' + props.fullPage ? 'preloader-fullpage' : ''}>
      <Sugar animation="fade" className="planets"/>
    </div>
  );
};

export default Preloader;