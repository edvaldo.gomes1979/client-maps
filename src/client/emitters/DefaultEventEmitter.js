import React from 'react';
import { EventEmitter } from 'fbemitter'

const emitter = new EventEmitter();
export default emitter;