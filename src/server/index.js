const dotenv  = require('dotenv').config();
const morgan  = require('morgan');

const express    = require('express');
const bodyParser = require('body-parser');

let   app = express();

const config   = require('./config/config');
const database = require('./config/mongodb');
const session  = require('./config/session')(app);

app.use(morgan('combined'));
app.use(express.static('dist'));
// app.set('view engine', 'pug')

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app = require('./config/app')(app);

const routes   = require('./config/routes');
app.use(routes);

app.listen(config.app.port, () => console.log(`Listening on port ${config.app.port}!`));