const queryParseTypeGroupHelper = (filter) => {
  filter = JSON.parse(filter);
  
  if(filter.zoom <= 6){
    return 'state';
  }

  if(filter.zoom <= 11){
    return 'city';
  }

  return 'district';
};

module.exports = queryParseTypeGroupHelper;