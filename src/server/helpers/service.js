const config = require('../config/config');
const axios  = require('axios');

class Service{  
  static get(service, params){
    let url = this._url(service);
    return axios.get(url, {params: params, headers: this._headers()});
  }

  static post(service, data){
    let url = this._url(service);
    return axios.post(url, data);
  }

  static _url(service){
    return `${config.service.url}/${config.service.namespace}/${service}`;
  }

  static _headers(){
    return {
      'authorization': config.service.authorization_key
    };
  }
}

module.exports = Service;