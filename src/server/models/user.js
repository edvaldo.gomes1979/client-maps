const UserSchema = require('../schema/user');

class User{

  /**
   * Faz a autenticação do usuário 
   *
   * @param  { Object }   data Os dados de autenticação retornados pelo Google OAuth API
   * @return { Object || Boolean }
   * 
   */
  static async signIn(data){
    let user = await UserSchema.findOne({
      google_id: data.profileObj.googleId,
      email:     data.profileObj.email,
      status:    true
    }).exec();

    if(!user){      
      return false;
    }

    user.last_access  = new Date();
    user.access_count = !user.access_count ? 1 : user.access_count + 1;
    user = this.update(user, data)  
    return user;
  }

  /**
   * Atualiza os dados do usuário
   * 
   * @param { Object } UserSchema registro no banco do usuário
   * @param { Object } data Os dados de autenticação retornados pelo Google OAuth API;
   * @retun { Bolean }
   * 
   */
   static async update(user, data){
      user.access_token = data.tokenObj.access_token;
      user.expires_at   = data.tokenObj.expires_at;
      user.tokenId      = data.tokenObj.tokenId;
      user.name         = data.profileObj.name;
      user.avatar       = data.profileObj.imageUrl;

      user.save();
      return user;
   }
}

module.exports = User;