const redis       = require('../helpers/redis');
const ExamSchema  = require('../schema/exam');

class Exam{
  /**
   * Obtem o total de exames existentes para a condição
   *
   * @param      { Object }   options  {filter: Object, limit: Number, skip: Number}
   * @return     { Number }   exams    Number
   * 
   */
  static async total(options){
    let cache = await this._getCache(options, 'total');

    if(cache !== null){
      return cache;
    }

    let total = await ExamSchema.countDocuments(options.filter);
    this._setCache(options, total, 'total');    

    return total;
  }

  /**
   * Obtem as geolocalizações dos resultados de exames
   *
   * @param      { Object }   options  {filter: Object, limit: Number, skip: Number}
   * @return     { Array }    exams    [[Float, Float, Float]]
   * 
   */
  static async result(options){
    let cache = await this._getCache(options),
        exams = [];

    if(cache){
      return cache;
    }

    exams = await ExamSchema.find(options.filter, null, {limit: options.limit, skip: options.skip, sort: {_id: 1}});
    exams = exams.map((exam) => { return [exam.geo2.lat, exam.geo2.lon, 1]; });

    this._setCache(options, exams);    
    return exams;
  }

  /**
   * Obtem as geolocalizações dos resultados de exames como stream
   *
   * @param      { Object }  options  {filter: Object}
   * @return     { Promise }
   * 
   */
  static stream(options){
    return ExamSchema.find(options.filter).stream();
  }

  /**
   * { item_description }
   */
  static async aggregate(options, key = 'district'){
    options.type = key;

    let cache = await this._getCache(options, 'aggregate'),
        exams = [];

    if(cache){
      return cache;
    }

    key = {
      city:  {
        key: 'city',
        name: 'Cidades'
      },
      district: {
        key: 'city_district',
        name: 'Bairros'
      },
      state: {
        key: 'state',
        name: 'Estados'
      }
    }[key];

    let pipeline = [
      {$match: options.filter},
      {
        $group: {
          _id: `$patient_address.geolocation.address.${key.key}`,
          total: {$sum: 1}
        }
      },
      {$sort: {total: -1}}
    ];

    let data = await ExamSchema.aggregate(pipeline).exec();
    data = {
      type: key.name,
      data: this.formatToGraph(data),
    };

    this._setCache(options, data, 'aggregate');    

    return data;
  }

  /**
   * Faz a formatação dos agrupamentos para
   *
   * @param      {<type>}   options  The options
   * @return     {Promise}  The cache.
   */
   static formatToGraph(data){
    let colors = ['#7cb4ec', '#434348', '#90ec7e', '#f7a35c', '#8085e9', '#f15c80', '#e4d254', '#2a908f', '#f45b5b', '#CCCCCC'],
        formatted;

    return data.filter(i => i._id != null)
               .map((item, i) => {
                  if(!item._id){
                    return;
                  }

                  formatted = {};
                  formatted.name  = item._id ? item._id : 'Outros';
                  formatted.y     = item.total;
                  formatted.color = colors[i];

                  return formatted;
                })
               .splice(0, 10);
   }

  static async _getCache(options, namespace = 'stream'){
    let key = `${namespace}:${redis.key(options)}`;
    return await redis.get(key);
  }

  static async _setCache(options, data, namespace = 'stream'){
    let key = `${namespace}:${redis.key(options)}`;
    redis.set(key, data);
  }
}

module.exports = Exam;