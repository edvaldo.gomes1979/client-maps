const async = require('async');
const redis = require('../helpers/redis');
const service = require('../helpers/service');

const RegionSchema = require('../schema/region');
const Exam         = require('./exam');

class Region{
   
  static async all(filter = {}, densityFilter = {}, cb){
    let regions = await this._all(filter);
    let i, total, response = [];

    async.each([{type: 'all'}].concat(regions), async (item) => {
      if(item.type === 'region'){
        let result = await this.total(densityFilter, item.regions);
        response = response.concat(result.data.regions);
      }else{
        let result = await this.total(densityFilter, 'total');
        total = result.data.total;
      }
    }, () => {
      cb(response, total);
    });
  }

  static async total(filter, regions){
    return await service.get('regions/geometries', {filter: filter, regions: regions});
  }

  static async _all(filter){
    let cache = await this._getCache(filter, 'regions');

    // if(cache){
    //   return cache;
    // }

    let regions = await RegionSchema.find(filter).exec();
    let length    = 5,
        formatted = [],
        chunk     = [],
        i;

    for(i in regions){
      chunk.push(regions[i]._id.toString());

      if(chunk.length === length){
        formatted.push({regions: chunk, type: 'region'});
        chunk = [];
        continue;
      }
    }

    if(chunk.length){
      formatted.push({regions: chunk, type: 'region'});
    }

    regions = formatted;


    this._setCache(filter, regions, 'regions');  

    return regions; 
  }

  static geometry(region){
    return region.geometry;
    region = JSON.parse(JSON.stringify(region));
    region.geometry.coordinates = region.geometry.coordinates.map((coordinates) => {
      return coordinates.map((item) => {
        return item.sort();
      });
    });

    return region.geometry;
  }

  static async _getCache(options, namespace = 'regions'){
    let key = `${namespace}:${redis.key(options)}`;
    return await redis.get(key);
  }

  static async _setCache(options, data, namespace = 'regions'){
    let key = `${namespace}:${redis.key(options)}`;
    redis.set(key, data);
  }
}

module.exports = Region;