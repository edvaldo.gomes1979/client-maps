const PlaceSchema = require('../schema/place');

class Place{

  /**
   * Obtem todos os locais (Endereços do Fleury)
   * 
   * @param  { Object }  Condição
   * @return { Object || Boolean }
   * 
   */
  static async all(filter = {}){
    let places = await PlaceSchema.find(filter).exec();
    return places;
  }
}

module.exports = Place;