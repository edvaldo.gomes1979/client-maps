const mongoose = require("mongoose");

//Employee Model without any fixed schema
const RegionSchema = new mongoose.Schema({
    name:     mongoose.Schema.Types.String,
    geometry: mongoose.Schema.Types.Mixed,
    lat:      mongoose.Schema.Types.String,
    lon:      mongoose.Schema.Types.String,
    address:  mongoose.Schema.Types.String
  },
  {strict:false, collection: 'regions'}
);

module.exports = mongoose.model('Region', RegionSchema);