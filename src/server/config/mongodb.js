const config = require('./config');

const mongoose = require('mongoose');
mongoose.connect(config.mongodb.url, {useNewUrlParser: true});
mongoose.set('debug', true);


module.exports = mongoose;