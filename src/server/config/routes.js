const path   = require('path');
const config = require('./config');

const routes = require('express').Router();

// Controllers Require
const ExamPlacesController        = require('../controllers/exam/places');
const ExamResultController        = require('../controllers/exam/results');
const UserAuthController          = require('../controllers/users/auth');
const RegionsGeometriesController = require('../controllers/regions/geometries');
const StatsReportController       = require('../controllers/stats/report');


//Set Routes
// Regions
routes.get('/api/regions/geometries',  RegionsGeometriesController.index);

//Locais
routes.get('/api/exam/places',  ExamPlacesController.index);

// Exames / Resultados
routes.get('/api/exam/each-results', ExamResultController.each);
routes.get('/api/exam/results',      ExamResultController.index);
routes.get('/api/exam/regions',      ExamResultController.regions);

// Users
routes.post('/api/users/sign_in',   UserAuthController.sign_in);
routes.get('/api/users/logged_in',  UserAuthController.logged_in);
routes.get('/api/users/logout',         UserAuthController.logout);

// Stats Report
routes.get('/api/stats/report/:exam', StatsReportController.exam);


routes.get('*', (request, response) => {
  if(config.app.env === 'production'){
    return response.sendFile(path.resolve('./dist/index.html'));
  }

  return response.sendFile(path.resolve('./public/index.html'));
});

module.exports = routes;