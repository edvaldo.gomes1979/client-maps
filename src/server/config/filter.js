module.exports = {
  patient_gender:  {
    name:  'patient_gender',
    label: 'Sexo',
    category: 'clinical_notes',
    options: [
      {label: 'F', value: 'F', color: 'bg-pink-400'},
      {label: 'M', value: 'M', color: 'bg-light-blue-400'}
    ]
  },

  patient_age: {
    name:  'patient_age',
    label: 'Idade',
    category: 'clinical_notes',
    options: [
      {label: '0 - 45',  value: {'$gte': 0,  '$lte': 45}},
      {label: '46 - 65', value: {'$gte': 46, '$lte': 65}},
      {label: '66 - 80', value: {'$gte': 66, '$lte': 80}},
      {label: '81 - 90', value: {'$gte': 81, '$lte': 90}},
      {label: '> 90',    value: {'$gte': 90}}
    ],
    color: 'bg-yellow-800'
  },

  triglicerides: {
    name:  'medians.triglicerides.triglicerides_value',
    label: 'Triglicérides',
    category: 'exam',
    options: [
      {label: '< 150',     value: {'$lt': 150}},
      {label: '150 - 170', value: {'$gte': 150, '$lte': 170}},
      {label: '171 - 400', value: {'$gte': 171, '$lte': 400}},
      {label: '> 400',     value: {'$gt': 400}},
    ],
    color: 'bg-green-400'
  },

  colesterol_ldl: {
    name:  'medians.colesterol_ldl.colesterol_ldl_value',
    label: 'LDL',
    category: 'exam',
    options: [
      {label: '< 70',      value: {'$lt': 70}},
      {label: '70 - 100',  value: {'$gte': 70,  '$lte': 100}},
      {label: '101 - 130', value: {'$gte': 101, '$lte': 130}},
      {label: '131 - 160', value: {'$gt': 131,  '$lte': 160}},
      {label: '161 - 190', value: {'$gt': 161,  '$lte': 190}},
      {label: '> 190',     value: {'$gt': 190}},
    ],
    color: 'bg-blue-400'
  },

  colesterol_hdl: {
    name:  'medians.colesterol_hdl.colesterol_hdl_value',
    label: 'HDL',
    category: 'exam',
    options: [
      {label: '< 45',    value: {'$lt': 45}},
      {label: '45 - 50', value: {'$gte': 45,  '$lte':  50}},
      {label: '> 50',    value: {'$gt': 50}},
    ],
    color: 'bg-red-400'
  },

  glicemia_jejum: {
    name:  'medians.glicemia_jejum.glicemia_jejum_value',
    label: 'Glicemia',
    category: 'exam',
    options: [
      {label: '< 100',     value: {'$lt': 100}},
      {label: '100 - 125', value: {'$gte': 100, '$lte':  125}},
      {label: '126 - 199', value: {'$gte': 126, '$lte':   200}},
      {label: '201 - 400', value: {'$gte': 200, '$lte':   400}},
      {label: ' > 400',    value: {'$gt': 400}},
    ],
    color: 'bg-cyan-400'
  },

  hemoglobina_glicada: {
    name:  'medians.hemoglobina_glicada.hemoglobina_glicada_value',
    label: 'Hemoglobina Glicada',
    category: 'exam',
    options: [
      {label: '0 - 4', value: {'$gte': 0, '$lte': 4}},
      {label: '5 - 6', value: {'$gte': 5, '$lte': 6}},
      {label: '> 6',   value: {'$gt': 6}},
    ],
    color: 'bg-teal-400'
  },
  
  bmi: {
    name:  'medians.bmi.bmi_value',
    label: 'IMC ',
    category: 'exam',
    options: [
      {label: '< 16',    value: {'$lt': 16}},
      {label: '16 - 17', value: {'$gte': 16, '$lte': 17}},
      {label: '18 - 19', value: {'$gte': 18, '$lte': 19}},
      {label: '20 - 25', value: {'$gte': 20, '$lte': 25}},
      {label: '26 - 30', value: {'$gte': 26, '$lte': 30}},
      {label: '31 - 35', value: {'$gte': 31, '$lte': 35}},
      {label: '> 40',    value: {'$gt': 40}},
    ],
    color: 'bg-purple-400'
  },

  clinical_notes_pressao_sistolica: {
    name:  'medians.clinical_notes_pressao.clinical_notes_pressao_sistolica',
    label: 'Pressão Arterial Sistólica ',
    category: 'exam',
    options: [
      {label: '< 120',     value: {'$lt': 120}},
      {label: '120 - 129', value: {'$gte': 120, '$lte': 129}},
      {label: '130 - 139', value: {'$gte': 130, '$lte': 139}},
      {label: '140 - 159', value: {'$gte': 140, '$lte': 159}},
      {label: '160 - 180', value: {'$gte': 160, '$lte': 180}},
      {label: '> 180', value: {'$gt': 180}},
    ],
    color: 'bg-grey-500'
  },

  clinical_notes_pressao_diastolica: {
    name:  'medians.clinical_notes_pressao.clinical_notes_pressao_diastolica',
    label: 'Pressão Arterial Diastólica ',
    category: 'exam',
    options: [
      {label: '< 80',      value: {'$lt': 80}},
      {label: '80 - 84',   value: {'$gte': 80, '$lte': 84}},
      {label: '85 - 89',   value: {'$gte': 85, '$lte': 89}},
      {label: '90 - 99',   value: {'$gte': 90, '$lte': 99}},
      {label: '100 - 110', value: {'$gte': 100, '$lte': 110}},
      {label: '> 110', value: {'$gt': 110}},
    ],
    color: 'bg-grey-400'
  }
};