const config = require('./config');

const redis  = require("async-redis"),
      client = redis.createClient({
        host:     config.redis.host,
        password: config.redis.password,
        db:       config.redis.db,
      });

module.exports = client;