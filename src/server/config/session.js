const config     = require('./config');
const session    = require('express-session');
const RedisStore = require('connect-redis')(session);

const redis = new RedisStore({
  host:     config.redis.host,
  password: config.redis.password,
  db:       config.redis.db,
});


module.exports = (app) => {
  app.use(session({
    store:   redis,
    secret:  config.app.secret,
    resave: false
  }));
};