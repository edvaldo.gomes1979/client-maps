module.exports = {
  app: {
    env:    process.env.APP_ENV,
    secret: process.env.APP_SECRET,
    port:   process.env.PORT
  },

  mongodb: {
    url: process.env.MONGODB_URL,
  },

  redis: {
    host:     process.env.REDIS_HOST,
    password: process.env.REDIS_PASSWORD,
    db:       parseInt(process.env.REDIS_DB, 10),
  },

  service: {
    url:               process.env.SERVICE_URL,
    namespace:         process.env.SERVICE_PATH,
    authorization_key: process.env.SERVICE_AUTHORIZATION_KEY
  }
};