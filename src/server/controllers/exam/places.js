const service = require('../../helpers/service');

class ExamPlacesController{

  // GET /api/exam/places
  static async index(request, response){
    service.get('exam/places', request.query)
           .then((result) => {
              response.send(result.data);
           });
  }


}

module.exports = ExamPlacesController;