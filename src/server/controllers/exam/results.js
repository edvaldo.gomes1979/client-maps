const exam              = require('../../models/exam');
const queryParseHelper  = require('../../helpers/query_parse');
const queryParseTypeGroupHelper  = require('../../helpers/query_parse_type_group');
const service = require('../../helpers/service');

class ExamResultController{

  // GET /api/exam/regions
  static async regions(request, response){
    service.get('exam/regions', request.query)
           .then((result) => {
              response.send(result.data);
           });
  }

  // GET /api/exam/results
  static async index(request, response, next){
    let stop = false;

    request.on("close", () => {
      response.end();

      stop = true;
    });

    response.writeHead(200, {'Content-Type': 'text/plain', 'Transfer-Encoding': 'chunked'});

    let options = {
      filter: queryParseHelper(request.query.filter),
      limit:  1000,
      skip:   0
    };

    let total  = await exam.total(options),
        exams  = [];

    let page = {
      total:   Math.ceil(total / options.limit),
      current: 0,
    };

    while(page.current < page.total){
      exams = await exam.result(options);

      options.skip += exams.length;
      page.current += 1;

      if(stop){
        return;
      }

      response.write(JSON.stringify({total: total, chunk: options.skip, data: exams}));
    }

    response.end();
  }

  // GET /api/exam/each-results
  static async each(request, response, next){
    let stop = false;

    response.writeHead(200, {'Content-Type': 'text/plain', 'Transfer-Encoding': 'chunked'});

    let options = {
      filter: queryParseHelper(request.query.filter),
      chunks: 0
    };

    let total = await exam.total(options),
        exams = exam.stream(options);

    exams.on('data', function(item){
      options.chunks += 1;

      if(stop){ return; }
      response.write(JSON.stringify({total: total, chunk: options.chunks, data: [[item.geo2.lat, item.geo2.lon, 1]]}));
    });

    let closeConnection = () => {
      if(stop){ return; }
      response.end();

      stop = true;
      exams.destroy()
    };

    exams.on('error', closeConnection);
    exams.on('close', closeConnection);
    request.on("close", closeConnection);
  }
}

module.exports = ExamResultController;