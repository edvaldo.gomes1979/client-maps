const config         = require('../../config/filter');

const queryParse     = require('../../helpers/query_parse');
const aggregateParse = require('../../helpers/aggregate_parse');

class ReportController{

  static index(request, response){
    response.render('stats/report/index');
  }

  static exam(request, response){
    console.log('________________________/////////////////////////////////////____________________________');
    console.log('/////////////////////////////////////___________________/////////////////////////////////////');
    console.log(
      JSON.stringify(
        aggregateParse({
          exam:    request.params.exam,
          origin:  'fleury_checkup',
          filter:  {}
        })
      )
    );


    response.send({
      categories: ['FLEURY', 'FLEURY CHECKUP', 'HSP'],
      series:[
        {
          name: '0 - 45',
          data: [130, 454, 35],
          color: 'rgba(249, 168, 37, 0.2)'
        },
        {
          name: '46 - 65',
          data: [12, 111, 44],
          color: 'rgba(249, 168, 37, 0.4)'
        },
        {
          name: '66 - 80',
          data: [12, 111, 44],
          color: 'rgba(249, 168, 37, 0.6)'
        },
        {
          name: '81 - 90',
          data: [12, 111, 54],
          color: 'rgba(249, 168, 37, 0.8)'
        },
        {
          name: '> 90',
          data: [12, 111, 524],
          color: 'rgba(249, 168, 37, 1)'
        }
      ]
    });
  }
}

module.exports = ReportController;