const service = require('../../helpers/service');
const queryParseHelper  = require('../../helpers/query_parse');
const Region  = require('../../models/region');

class RegionsGeometriesController{
  // GET /regions/geometries
  static async index(request, response){    
    Region.all({}, request.query.filter, (regions, total) => {
      response.send({regions: regions, total: total});
    });
  }
}

module.exports = RegionsGeometriesController;