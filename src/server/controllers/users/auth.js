const User = require('../../models/user');

class UserAuthController{
  static async sign_in(request, response){
    let user = await User.signIn(request.body);

    if(user){
      request.session.current_user = user;
      return response.send(user);
    }

    response.send({status: false, message: "O usuário não possui permissão para acesso."});
  }

  static async logged_in(request, response){
    let user = request.session.current_user;

    if(user){
      return response.send(user);
    }

    response.send({status: false, message: "O usuário não está logado."});
  }

  static async logout(request, response){
    if(request.session.current_user){
      request.session.current_user = null;
    }

    return response.redirect('/')
  }
}

module.exports = UserAuthController;