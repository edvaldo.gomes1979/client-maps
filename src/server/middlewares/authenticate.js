const alloweds = [
  '/users/sign_in',
  '/api/users/sign_in',
  '/api/users/logged_in'
];


module.exports = (app) => {
  app.use((request, response, next) => {
    let notAllowed = !request.session.current_user && alloweds.indexOf(request.path) === -1;
        // notAllowed = notAllowed || (request.session.current_user && request.session.current_user.expires_at < (new Date()).getTime()) &&  alloweds.indexOf(request.path) === -1;;

    if(notAllowed){
      request.session.current_user = false;
      return response.status(401)
                     .redirect('/users/sign_in');
    }
    
    next();
  });

  return app;
};